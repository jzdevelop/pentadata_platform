import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainCardsComponent } from './components/main-cards/main-cards.component';
import { ChartComponent } from './components/chart/chart.component';
import { LoginComponent } from './components/login/login.component';
import {AuthGuard} from './auth.guard'
import { SoporteComponent } from './components/soporte/soporte.component';
const routes: Routes = [
  { path: 'home', component: MainCardsComponent, canActivate: [AuthGuard] },
  { path: 'chart',      component: ChartComponent, canActivate: [AuthGuard] },
  { path: '', component: MainCardsComponent, canActivate:[AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'soporte', component: SoporteComponent, canActivate:[AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
