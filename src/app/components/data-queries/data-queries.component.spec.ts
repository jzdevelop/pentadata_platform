import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataQueriesComponent } from './data-queries.component';

describe('DataQueriesComponent', () => {
  let component: DataQueriesComponent;
  let fixture: ComponentFixture<DataQueriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataQueriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataQueriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
