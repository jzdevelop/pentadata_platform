import { Component, OnInit } from '@angular/core';
//import {DataFireService} from '../../services/data-fire.service'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { formatDate } from '@angular/common';
import { DataFireService } from '../../services/data-fire.service'
@Component({
  selector: 'app-main-cards',
  templateUrl: './main-cards.component.html',
  styleUrls: ['./main-cards.component.css']
})
export class MainCardsComponent implements OnInit {
  puntosVenta: any = [];
  // estos arreglos son salesPlace para almacenar el nombre del punto venta como viene de la bd
  // auxSalesPlace guarda el punto de venta organizado para mejor visualización
  salesPlace: string[] = [];
  auxSalesPlace: string[] = [];
  existSalesPlce: any = [];
  //arr1: boolean[] = [];
  constructor(public db: AngularFirestore, public dataFireService: DataFireService) { }

  ngOnInit() {
    this.puntosVenta.length = 0;
    this.salesPlace.length = 0;
    this.auxSalesPlace.length = 0;
    // la promesa en angular se remplaza por subscribe y funciona igual
    this.db.collection('FrancorpPuntosVenta').get().subscribe((res) => {
      res.forEach((doc) => {
        // variable para garantizar el tipo de dato obtenido desde la db
        let puntoVenta: string = "" + doc.data().puntoVenta;
        // como en la bd hay puntos de venta repetidos
        // se pregunta si el punto de venta ya existe y no se crea el card
        if (!this.auxSalesPlace.includes(puntoVenta)) {
          // organiza los datos para la visualización
          let dataFromDB: any = {
            fecha: this.getFormatedDate(doc.data().fecha),
            humedad: doc.data().humedad,
            humedadAmbiente: doc.data().humedadAmbiente,
            puntoVenta: this.getFormatedPuntoVenta(doc.data().puntoVenta),
            temperatura: doc.data().temperatura,
            temperaturaAmbiente: doc.data().temperaturaAmbiente,
            logo: this.getLogo(doc.data().puntoVenta)
          }
          this.puntosVenta.push(dataFromDB);
          // se guarda el punto de venta sin dar formato
          this.auxSalesPlace.push(doc.data().puntoVenta);

          // se guarda el punto de venda con formato
          this.salesPlace.push(this.getFormatedPuntoVenta(doc.data().puntoVenta));
        }
      });
    });
  }
  // método que devuelve el punto de venta separado por espacio para meor visualización
  getFormatedPuntoVenta(puntoVenta: any) {
    let formatedString: string = "" + puntoVenta;
    formatedString = formatedString.substr(0, 1).toUpperCase() + formatedString.substr(1, formatedString.length - 1);
    for (let i = 0; i < formatedString.length; i++) {
      if (formatedString[i] == "_") {
        formatedString = formatedString.slice(0, i) + " " + formatedString.slice(i + 1, i + 2).toUpperCase() + formatedString.slice(i + 2);
      }
    }
    return formatedString;
    //return mystring.split(',').join(newchar);
  }
  getFormatedDate(date: any) {
    let formatedDate = "" + date;
    formatedDate = formatedDate.slice(0, 4) + "-" + formatedDate.slice(4, 6) + "-" + formatedDate.slice(6, 8) + " " + formatedDate.slice(8, 10) + ":" + formatedDate.slice(10);
    return formatedDate;
  }
  // pasa la dirección de la imagen correspondiente segun el punto de venta
  getLogo(logo: any) {
    let franquicia: string;
    franquicia = logo;
    if (franquicia.slice(0, 6) == "frisby") {
      return 'assets/img/frisby.svg';
      //console.log(true);
    }
    else {
      return 'assets/img/sarku.png';
      //console.log(false);
    }
  }
  // se obtiene el punto de venta que se hizo click
  getPuntoVentaClick(puntoVentaFromCard: any) {
    let puntoVenta: string = puntoVentaFromCard
    //console.log("Sales Place: " + puntoVenta);
    //this.dataFireService.nombrePuntoVenta = puntoVentaFromCard;
    for (let i = 0; i < this.auxSalesPlace.length; i++) {

      //console.log("SalesPlace: " + this.salesPlace[i]);
      //console.log("auxSalesPlace: " + this.auxSalesPlace[i]);
      if (this.salesPlace[i] == puntoVenta) {
        // actualizo las variables públicas del servicio
        // con los datos del punto de venta
        this.dataFireService.nombrePuntoVenta = puntoVenta;
        this.dataFireService.nombrePuntoVentaWithoutFormat = this.auxSalesPlace[i];
        break;
      }
    }
    //console.log("PuntoVenta: " + puntoVenta);
  }

}
