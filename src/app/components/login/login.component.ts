import { Component, OnInit, Input } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { Router} from '@angular/router';
import * as firebase from 'firebase/app';
import 'firebase/auth';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router:Router) { 
    firebase.initializeApp(this.config);
  }
  // obtengo los datos ingresados por el usuario
  @Input('name') email:string;
  @Input('name') password:string;
  //password:string;
  db:any; // referencia a la base de datos
  config: any = {
    apiKey: "AIzaSyBcZHiaO4l9J_cRcDMXqMoAbgB2XKSWDNA",
    authDomain: "smet-d8c83.firebaseapp.com",
    databaseURL: "https://smet-d8c83.firebaseio.com",
    projectId: "smet-d8c83",
    storageBucket: "smet-d8c83.appspot.com",
    messagingSenderId: "895530049495"
  };
  auth:any;
  ngOnInit() {
    
  }

  loginUser(form:NgForm){
    console.log(form.value.password);
    this.auth = firebase.auth();
    this.auth.signInWithEmailAndPassword(form.value.email, form.value.password).then(()=>{
      localStorage.setItem('token','login');
      this.router.navigate(['/home']);
      //console.log(localStorage.getItem('token'));
    }).catch((error:any)=>{
      
      localStorage.setItem('token','logout');
    });
    
  }

}
