import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
//import { ChartDataSets, ChartOptions } from 'chart.js';
import { Chart } from 'chart.js';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import * as moment from 'moment-timezone';
import {DataFireService} from '../../services/data-fire.service';
//import { ResizedEvent } from 'angular-resize-event';
@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  lineChart: any;
  dataTemperatura: number[] = [];
  dataHumedad: number[] = [];
  dataPuertaAbierta: number[] = [];
  dataFecha: string[] = [];
  dataTemperaturaIdeal: number[] = [];
  dataTempLimiteInferior: number[] = [];
  //esta variable almacena todas las zonas donde se mide (cava, cuarto frio, ...)
  dataZonasMedicion:string[] = [];
  //variable que almacena el id de la zona de medicion el cual es el que sive como parámetro para las consultas
  auxDataZonasMedicion:string[] = [];
  // zona de medicón, con esta variable se va haciendo la consulta segúnn la zona de medición
  zonaMedicion:string = "pd001";
  puntosVentaCollection: AngularFirestoreCollection;

  constructor(public db: AngularFirestore, public dataFireSerive: DataFireService) {
    //console.log("Constructor: " + this.dataFireSerive.nombrePuntoVentaWithoutFormat);
  }

  ngOnInit() {
    //console.log("oninit: " + this.dataFireSerive.nombrePuntoVenta);
    this.dataFecha.length = 0;
    this.dataTemperatura.length = 0;
    this.dataHumedad.length = 0;
    this.dataPuertaAbierta.length = 0;
    this.dataTemperaturaIdeal.length = 0;
    this.dataTempLimiteInferior.length = 0;
    this.auxDataZonasMedicion.length = 0;
    this.dataZonasMedicion.length = 0;
    //console.log("chart component");
    //obtiene los primeros 10 minutos de datos
    this.getUltimosDiezMinutos(this.zonaMedicion);
    this.getZonasMedicion();
    //obtiene las zonas de medición del punto de venta
    
  }
  /*este método se ejecuta para seleccionar la zona de medición y hacer update a la variable
  auxDataZonaMedicion la cual va a ser la encargada de realizar servir cómo parámetro para las consultas*/
  seleccionarZonaDeMedicion(zona:string){
    //let zonaMedicion: string = zona;
    //console.log("Sales Place: " + puntoVenta);
    //this.dataFireService.nombrePuntoVenta = puntoVentaFromCard;
    for (let i = 0; i < this.dataZonasMedicion.length; i++) {

      //console.log("SalesPlace: " + this.salesPlace[i]);
      //console.log("auxSalesPlace: " + this.auxSalesPlace[i]);
      if (this.dataZonasMedicion[i] == zona) {
        // actualizo las variables públicas del servicio
        // con los datos del punto de venta
        this.zonaMedicion = this.auxDataZonasMedicion[i];
        this.getUltimosDiezMinutos(this.zonaMedicion);
        break;
      }
    }
  }
  getUltimosDiezMinutos(lugarMedicion:string){
    this.dataFecha.length = 0;
    this.dataTemperatura.length = 0;
    this.dataHumedad.length = 0;
    this.dataPuertaAbierta.length = 0;
    this.dataTemperaturaIdeal.length = 0;
    this.db.collection('FrancorpDB', ref => ref.limit(5).where("puntoVenta","==","" + this.dataFireSerive.nombrePuntoVentaWithoutFormat).where("lugarMedicion", "==", lugarMedicion).orderBy("fecha", "desc")).get().subscribe((res) => {
      res.forEach(doc => {
        //console.log(doc.data());
        let fecha = "" + doc.data().fecha;
        this.dataTemperatura.push(doc.data().temperatura);
        this.dataFecha.push(fecha.slice(0, 4) + "/" + fecha.slice(4, 6) + "/" + fecha.slice(6, 8) + " " +
        fecha.slice(8, 10) + ":" + fecha.slice(10));
        this.dataHumedad.push(doc.data().humedad);
        this.dataPuertaAbierta.push(doc.data().tiempoPuertaAbierta);
        this.dataTemperaturaIdeal.push(4);
        this.dataTempLimiteInferior.push(0);
      });
      
      this.drawChart(this.dataTemperatura.reverse(), this.dataFecha.reverse(), this.dataHumedad.reverse(), this.dataPuertaAbierta.reverse(), this.dataTemperaturaIdeal, this.dataTempLimiteInferior);
    });
  }
  getZonasMedicion(){
    this.db.collection('FrancorpPuntosVenta', ref => ref.where("puntoVenta","==","" + this.dataFireSerive.nombrePuntoVentaWithoutFormat)).get().subscribe((res) => {
      res.forEach(doc => {
        //console.log(doc.data());
        this.dataZonasMedicion.push(doc.data().zonaMedicion);
        this.auxDataZonasMedicion.push(doc.data().lugarMedicion);
      });
      
      //this.drawChart(this.dataTemperatura.reverse(), this.dataFecha.reverse(), this.dataHumedad.reverse(), this.dataPuertaAbierta.reverse(), this.dataTemperaturaIdeal, this.dataTempLimiteInferior);
    });
  }
  //obtiene la fecha según rango
  getDate(range: string) {
    const date = moment().tz("America/Bogota").format();

    const year: string = date.slice(0, 4);
    //console.log(year);
    const month: string = date.slice(5, 7);
    //console.log(month);
    const day: string = date.slice(8, 10);
    const hour: string = date.slice(11, 13);

    if (range == "lastHour") {
      const dateNow: string = year + month + day + hour + "00";
      const dateInNumber: number = parseInt(dateNow);
      return dateInNumber;
    }
    else if (range == "lastDay") {

      const dateNow: string = year + month + day + "00" + "00";

      const dateInNumber: number = parseInt(dateNow);
      //console.log("date: " + dateInNumber);
      return dateInNumber;
    }
    else if (range == "lastWeek") {
      //let week:number = 0;
      if (parseInt(day) <= 7) {
        const week: number = 30 - Math.abs(parseInt(day) - 7);
        let newMonth: string;
        if (parseInt(month) == 1) {
          newMonth = "" + 12;
        }
        else {
          if (parseInt(month) <= 10) {
            newMonth = "0" + (parseInt(month) - 1);
          }
          else {
            newMonth = "" + (parseInt(month) - 1);
          }

        }
        const dateNow: string = year + newMonth + week.toString() + "00" + "00";
        const dateInNumber: number = parseInt(dateNow);
        return dateInNumber;
      }
      else {
        const week: string = "" + (parseInt(day) - 7);
        console.log("semana: " + week);
        const dateNow: string = year + month + week + "00" + "00";
        const dateInNumber: number = parseInt(dateNow);
        return dateInNumber;
      }
    }
    else if (range == "lastMonth") {
      const dateNow: string = year + month + "00" + "00" + "00";
      const dateInNumber: number = parseInt(dateNow);
      return dateInNumber;
    }
  }
  getLastHour() {
    const date: number = this.getDate("lastHour");
    this.getDataFromFirebase(date);
    //this.lineChart.update();
  }
  getLastDay() {
    const date: number = this.getDate("lastDay");
    this.getDataFromFirebase(date);
    //this.lineChart.update();
  }
  getLastWeek() {
    const date: number = this.getDate("lastWeek");
    this.getDataFromFirebase(date);
  }
  getLastMonth() {
    const date: number = this.getDate("lastMonth");
    this.getDataFromFirebase(date);
  }
  drawChart(dataTemperatura: any, labelsFecha: any, dataHumedad: any, dataPuertaAbierta: any, dataTemperaturaIdeal: any, dataTempLimiteInferior: any) {
    let context: any = document.getElementById("line-chart");
    this.lineChart = new Chart(context, {
      type: 'bar',

      // The data for our dataset
      data: {
        labels: labelsFecha,
        datasets: [{
          label: "Temperatura",
          yAxisID: 'Temperatura',
          fill: false,
          borderColor: '#00796b',
          backgroundColor: '#80cbc4',
          data: dataTemperatura,
          pointRadius: 3,
          type: 'line'
        }, {
          label: "Tiempo Puerta Abierta",
          yAxisID: 'Humedad',
          fill: false,
          borderColor: 'rgba(66, 165, 245, 1)',
          backgroundColor: '#bbdefb',
          data: dataPuertaAbierta,
          pointRadius: 3,
          type: 'line'
        }, {
          label: "Temp Límite superior",
          yAxisID: 'Temperatura',
          fill: false,
          borderColor: 'rgba(244, 67, 54, 1)',
          backgroundColor: '#ffab91',
          pointRadius: 1,
          data: dataTemperaturaIdeal,
          type: 'line'
        }, {
          label: "Temp Límete Inferior",
          yAxisID: 'Temperatura',
          fill: false,
          borderColor: 'rgba(255, 235, 59, 1)',
          backgroundColor: '#fff59d',
          pointRadius: 1,
          data: dataTempLimiteInferior,
          type: 'line'
        }, {
          fill: false,
          label: 'Humedad',
          yAxisID: 'Humedad',//borderColor: 'rgba(187, 222, 251, 1)',
          backgroundColor: 'rgba(227, 242, 253, 1)',
          data: dataHumedad,
          pointRadius: 3,
          type: 'bar'
        }]
      },

      // Configuration options go here
      options: {
        title: {
          display: true,
          text: this.dataFireSerive.nombrePuntoVenta,
          fontSize: 16
        },
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Temperatura (°C)',
              fontStyle: 'bold',
              fontSize: 14
            },
            ticks: {
              beginAtZero: true
            },
            id: 'Temperatura',
            type: 'linear',
            position: 'left'
          }, {
            scaleLabel: {
              display: true,
              labelString: 'Humedad (%) - Puerta Abierta (s)',
              fontStyle: 'bold',
              fontSize: 14
            },
            id: 'Humedad',
            type: 'linear',
            position: 'right'
          },
          ]
        }
      }
    });
    //this.lineChart.update();
  }
  removeChart() {
    //destruye el chart
    this.lineChart.destroy();
    let element = document.getElementById("line-chart");
    element.parentNode.removeChild(element);
  }
  addChart() {
    let linechart = document.getElementById("chart");
    let newElement = document.createElement("canvas");
    newElement.setAttribute('id', "line-chart");
    newElement.innerHTML = `<canvas id="line-chart"></canvas>`;
    linechart.appendChild(newElement);
  }
  getDataFromFirebase(fecha: number) {
    this.dataFecha.length = 0;
    this.dataTemperatura.length = 0;
    this.dataHumedad.length = 0;
    this.dataPuertaAbierta.length = 0;
    this.dataTemperaturaIdeal.length = 0;
    this.dataTempLimiteInferior.length = 0;
    this.db.collection('FrancorpDB', ref => ref.where("lugarMedicion", "==", this.zonaMedicion).where("puntoVenta","==",this.dataFireSerive.nombrePuntoVentaWithoutFormat).where("fecha", ">=", fecha).orderBy("fecha", "desc")).get().subscribe((res) => {
      res.forEach(doc => {
        //console.log(doc.data());
        if (doc.data().temperatura < - 50) {
          //no hace nada si hay una medición mal realizada
        }
        else {
          /* let anio = "" + doc.data().fecha.slice(0,4) + "/";
          let mes = "" + doc.data().fecha.slice(4,6) + "/";
          let dia = "" + doc.data().fecha.slice(6,8) + " ";
          let hora = "" + doc.data().fecha.slice(8,10) + ":";
          let minuto = "" + doc.data().fecha.slice(10); */
          let fecha = "" + doc.data().fecha;
          this.dataTemperatura.push(doc.data().temperatura);
          this.dataPuertaAbierta.push(doc.data().tiempoPuertaAbierta);
          this.dataFecha.push(fecha.slice(0, 4) + "/" + fecha.slice(4, 6) + "/" + fecha.slice(6, 8) + " " +
            fecha.slice(8, 10) + ":" + fecha.slice(10));
          this.dataHumedad.push(doc.data().humedad);
          this.dataTemperaturaIdeal.push(4);
          this.dataTempLimiteInferior.push(0);
        }
      });
      this.removeChart();
      this.addChart();
      this.drawChart(this.dataTemperatura.reverse(), this.dataFecha.reverse(), this.dataHumedad.reverse(), this.dataPuertaAbierta.reverse(), this.dataTemperaturaIdeal, this.dataTempLimiteInferior);
      //this.lineChart.update();
    });
  }
  getFormatedPuntoVenta(puntoVenta: any) {
    let formatedString:string = "" + puntoVenta;  
    formatedString = formatedString.substr(0,1).toUpperCase() + formatedString.substr(1, formatedString.length-1);
    for(let i=0; i<formatedString.length; i++){
      if(formatedString[i] == "_"){
        formatedString = formatedString.slice(0,i) + " " + formatedString.slice(i+1,i+2).toUpperCase() + formatedString.slice(i+2);
      }
    }
    return formatedString;
    //return mystring.split(',').join(newchar);
  }

}
