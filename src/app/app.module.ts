import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainCardsComponent } from './components/main-cards/main-cards.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { DataQueriesComponent } from './components/data-queries/data-queries.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ChartComponent } from './components/chart/chart.component';
import { ChartsModule } from 'ng2-charts';
import { RouterModule, Routes } from '@angular/router';
import {MatIconModule} from '@angular/material/icon';
import { LoginComponent } from './components/login/login.component';
import { SoporteComponent } from './components/soporte/soporte.component';
import {MatSelectModule} from '@angular/material/select';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './auth.guard';
import {FormsModule} from '@angular/forms'
const appRoutes: Routes = [
  { path: 'home', component: MainCardsComponent },
  { path: 'chart',      component: ChartComponent },
  { path: '', component: MainCardsComponent},
  {path: 'soporte', component: SoporteComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    MainCardsComponent,
    DataQueriesComponent,
    ChartComponent,
    LoginComponent,
    SoporteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserAnimationsModule,
    ChartsModule,
    MatIconModule,
    MatSelectModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [AngularFirestore, AuthService, AuthGuard],
  bootstrap: [AppComponent]

})
export class AppModule { }
