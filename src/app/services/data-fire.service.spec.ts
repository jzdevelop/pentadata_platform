import { TestBed } from '@angular/core/testing';

import { DataFireService } from './data-fire.service';

describe('DataFireService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataFireService = TestBed.get(DataFireService);
    expect(service).toBeTruthy();
  });
});
