export interface Product {
    id?: string; // el ? indica que es opcional
    name?:string;
    descripcion?: string;
    price?: number;
}