export interface PuntoVenta{
    fecha?: number;
    humedad?: number;
    humedadAmbiente?: number;
    lugarMedicion?: string;
    puntoVenta?: string;
    temperatura?: number;
    temperaturaAmbiente?: number;
}