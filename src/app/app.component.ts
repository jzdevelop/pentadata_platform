import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery'; //importación de jquery para el efecto de sidebar
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public togle = 1;
  ngOnInit(): void {
    $(document).ready(function () {
  
      let i = 1;
      $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        if($('#body').width() < 540){

        }
        else if(i == 1){
          $('#chart').width($('#container').width() + 200);
          //document.getElementById('chart').style.width = parseInt(document.getElementById('body').style.width) - parseInt(250);
          //console.log("cerrado: " + $( '#body').width() + "sidebar: " + $( '#sidebar').width());
          i=0;
        }
        
        else{
          $('#chart').width($('#container').width() - 200);
          console.log("abierto" + $( '#container').width());
          i = 1;
        }
        
        //document.getElementById('chart').style.width = document.getElementById('container').style.width;
      });
      /* Call the function */

    });

  }
  // onResized(event: ResizedEvent) {
  //   document.getElementById('chart').style.width = document.getElementById('container').style.width;
  //   console.log("resize: " + event.newWidth);
  //   //this.height = event.newHeight;
  // }
  title = 'pentadata-platform';
}
